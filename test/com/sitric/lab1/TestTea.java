package com.sitric.lab1;

import com.sitric.lab1.product.Product;
import com.sitric.lab1.product.Tea;
import org.junit.Assert;
import org.junit.Test;

public class TestTea {
    @Test
    public void testCreate(){
        Product tea = new Tea().create();
        Assert.assertNotNull(((Tea) tea).getPack());
    }

    @Test
    public void testDelete(){
        Product tea = new Tea().create();
        tea.delete();
        Assert.assertNull(tea.getId());
    }
}
