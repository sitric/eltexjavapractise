package com.sitric.lab5;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.sitric.lab5.product.Product;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
    класс-коллекция "корзина"
    считается, что корзина может сущестовать без связи с заказом
*/
@JsonAutoDetect
public class ShoppingCart implements Serializable {

    private List<Product> cartItems = new ArrayList<>();

    public List<Product> getCartItems() {
        return cartItems;
    }

    public void add(Product product){
        cartItems.add(product);
    }

    public void delete(Product product){
        cartItems.remove(product);
    }

    public void showAll(){
        for(Product product : cartItems) {
            product.read();
        }
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                /*"cartItems=" +*/ cartItems +
                '}';
    }
}
