package com.sitric.lab5.order;

/*
    Класс Orders предоставляет функционал для работы с Order
*/

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class Orders{
    private List<Order> orders = new LinkedList<>();

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void makeOrder(Order order){
        orders.add(order);
    }

    public void showOrders() {
        for(Order order: orders){
            System.out.println(order);
            order.getShoppingCart().showAll();
        }
    }

    public Order findOrderById(UUID id) {
        Order order = null;

        for(Order searchObj : orders) {
            if (searchObj.getId().equals(id)){
                order = searchObj;
                break;
            }
        }
        return order;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orders=" + orders +
                '}';
    }
}
