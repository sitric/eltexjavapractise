package com.sitric.lab5.order;

/*
 * Класс CheckPending запускается в отдельном потоке и меняет статус у заказов
 */

import java.util.Calendar;
import static java.lang.Thread.sleep;

public class CheckPending extends ACheck implements Runnable {
    private Orders orders;
    private final Object LOCKER;

    public CheckPending(Orders orders, Object LOCKER) {
        this.orders = orders;
        this.LOCKER = LOCKER;
    }


    @Override
    public void checkStatus() {
        synchronized (LOCKER) {
            for (Order order : orders.getOrders()) {
                if (order.getOrderStatus().equals("waiting") &&
                        (order.getCreationTime() + order.getWaitingTime() <= Calendar.getInstance().getTimeInMillis())
                        ) {
                    System.out.println("--UPDATE STATUS ORDER ID: " + order.getId());
                    order.setOrderStatus("done");
                }
            }
        }
    }

    @Override
    public void run() {
        boolean goAhead = true;
        while (goAhead) {
            try {
                if (orders.getOrders().size() > 0) {
                    checkStatus();
                }
                sleep(500);
            } catch (Exception e) {
                goAhead = false;
            }
        }
    }
}
