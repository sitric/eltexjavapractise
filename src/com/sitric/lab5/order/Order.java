package com.sitric.lab5.order;

/*
    класс "Заказ"
*/

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.sitric.lab5.Credentials;
import com.sitric.lab5.ShoppingCart;

import java.io.Serializable;
import java.util.Calendar;
import java.util.UUID;

@JsonAutoDetect
public class Order implements Serializable {
    private String orderStatus; //в ожидании, обработан
    private long creationTime;
    private long waitingTime; // время, через которое заказ должен исчезнуть
    private UUID id;

    private ShoppingCart shoppingCart;
    private Credentials credentials;

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Order(ShoppingCart cart, Credentials credentials) {
        this.orderStatus = "waiting";
        this.creationTime = Calendar.getInstance().getTimeInMillis();
        this.waitingTime = (long)(Math.random() * 1000);

        this.shoppingCart = cart;
        this.credentials = credentials;
        this.id = UUID.randomUUID();

    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public long getWaitingTime() {
        return waitingTime;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderStatus='" + orderStatus + '\'' +
                ", waiting time='" + waitingTime + '\'' +
                ", id=" + id +
                ", shoppingCart=" + shoppingCart +
                ", credentials=" + credentials +
                '}';
    }
}
