package com.sitric.lab5.orderManagement;

/*
 *
 * Класс, предоставляющий методы для записи в файл и извлечения из файла заказов в формате JSON
 *
*/

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sitric.AppConst;
import com.sitric.lab5.order.Order;
import com.sitric.lab5.order.Orders;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import static java.io.File.separator;

public class ManagerOrderJSON implements IOrder{
    static File prepareLogFile() throws IOException {

        String filepath = Paths.get("").toAbsolutePath().toString() + separator + "log";
        File logDir = new File(filepath);
        File logFile = new File(filepath + separator + AppConst.JSON_FILENAME);

        if (!logDir.exists()){
            logDir.mkdirs();
        }

        if (!logFile.exists()){
            logFile.createNewFile();
        }
        return logFile;
    }

    @Override
    public Order readById(UUID id) {
        Order order = null;
        List<Order> orders = readAll();
        if (orders == null) {
            System.out.println("--error! No elements found");
        }
        else {
            for (Order tempOrder : orders) {
                if (tempOrder.getId().equals(id)){
                    order = tempOrder;
                    break;
                }
            }
        }

        return order;
    }

    @Override
    public void saveById(Orders orders, UUID id) {
        Order order = null;
        for (Order tempOrder: orders.getOrders()){
            if (tempOrder.getId().equals(id)){
                order = tempOrder;
                break;
            }
        }
        System.out.println("--order with ID: " + id + " not found in file with JSON objects");
        if (order != null) {
            List<Order> ordersToSave = readAll();
            if (ordersToSave == null) {
                ordersToSave = new ArrayList<>();
            }
            ordersToSave.add(order);
            System.out.println("--Order with ID: " + id + " successfully added");
            saveAll(ordersToSave);

        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Order> readAll() {

        List<Order> ordersList = null;
        ObjectMapper mapper = new ObjectMapper();


        try(FileInputStream fis = new FileInputStream(prepareLogFile())){
            if (fis.available() != 0) {
                ordersList = (ArrayList<Order>) mapper.readValue(fis, List.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ordersList;
    }

    @Override
    public void saveAll(List<Order> orders) {
        try(FileOutputStream fos = new FileOutputStream(prepareLogFile(), false))
        {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(fos, orders);


            System.out.println("--all objects successfully saved in JSON");
        } catch (IOException e) {
            System.out.println("--error during saving objects in JSON");
            e.printStackTrace();
        }
    }
}
