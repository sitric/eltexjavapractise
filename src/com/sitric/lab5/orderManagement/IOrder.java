package com.sitric.lab5.orderManagement;

import com.sitric.lab5.order.Order;
import com.sitric.lab5.order.Orders;

import java.util.List;
import java.util.UUID;

interface IOrder {
    Order readById(UUID id);
    void saveById(Orders orders, UUID id);
    List<Order> readAll();
    void saveAll(List<Order> orders);
}
