package com.sitric.lab5;
/*
 * Согласно условиям лабораторной работы #5  реализовано:
 * 1. Класс  ManagerOrderFile для хранения заказов в виде двоичного файла
 * 2. Реализовать класс  ManagerOrderJSON для хранения заказов в виде текстового файла в формате JSON
*/
import com.sitric.lab5.order.*;
import com.sitric.lab5.orderManagement.ManagerOrderFile;
import com.sitric.lab5.orderManagement.ManagerOrderJSON;

import static java.lang.Thread.sleep;

public class Solution{
    private static final Object LOCKER = new Object();

    public static void main(String[] args) throws Exception {
        Orders orders = new Orders(); // common resource

        Thread ordersThread = new Thread(new OrderAutoGeneration(orders, LOCKER));

        //создадим несколько заказов
        ordersThread.start();

        sleep(1000);
        ordersThread.interrupt();

        //сохраним в бинарном виде
        ManagerOrderFile manageFile = new ManagerOrderFile();
        manageFile.saveAll(orders.getOrders());

        //сохраним в формате JSON
        ManagerOrderJSON manageJSON = new ManagerOrderJSON();
        manageJSON.saveAll(orders.getOrders());
    }

}
