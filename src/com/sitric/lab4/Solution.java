package com.sitric.lab4;
/*
 * Согласно условиям лабораторной работы #4 (является продолжением ЛР#3) реализовано:
 * 1. Абстрактный класс ACheck, описывающий проверку заказов по статусу
 * 2. Класс автоматической генерации заказов OrderAutoGeneration в виде отдельного потока
 * 3. Класс CheckPending в виде отдельного потока, проверяющий выполняется ли условие позволяющее считать заказ
 *    исполненным и меняющий статус заказа
 * 4. Класс CheckIsDone в виде отдельного потока, удаляющий из списка заказов те, которые имеют статус "done"
*/

import com.sitric.lab4.order.*;
import com.sitric.lab4.product.*;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.Thread.sleep;

public class Solution{

    private static final Object LOCKER = new Object();

    public static void main(String[] args) throws Exception{
        Orders orders = new Orders(); // common resource

        Thread ordersThread = new Thread(new OrderAutoGeneration(orders, LOCKER));
        Thread pendingThread = new Thread(new CheckPending(orders, LOCKER));
        Thread isDoneThread = new Thread(new CheckIsDone(orders, LOCKER));

        ordersThread.start();
        pendingThread.start();
        isDoneThread.start();

        sleep(4000);
        ordersThread.interrupt();
        pendingThread.interrupt();
        isDoneThread.interrupt();

        System.out.println();
        System.out.println("--ORDERS IN WORK:");
        orders.showOrders();
    }




}
