package com.sitric.lab4.product;

public class Tea extends Product {
    private String pack;

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public Tea() {
    }

    public Tea(String title, double price, String producer, String country, String pack) {
        super(title, price, producer, country);
        this.pack = pack;

    }

    @Override
    public String toString() {
        return "Tea{" + super.toString() +
                "pack='" + pack + '\'' +
                '}';
    }


    public void update(String title, double price, String producer, String country, String pack) {
        super.update(title, price, producer, country);
        setPack(pack);
    }

    @Override
    public Tea create() {
        String[] titles = {"Lipton", "Grienfield", "Princess Nouri", "Puer", "Tie Guan Yin"};
        String[] producer = {"Lipton ltd", "Famous Tea Company", "Multon"};
        double[] price = {100.00, 150.00, 200.00, 250.00, 300.00};
        String[] country = {"Russia", "India", "China"};
        String[] pack = {"cardboard", "wood"};

        this.update(titles[(int)(Math.random() * (titles.length))],
                price[(int)(Math.random() * (price.length))],
                producer[(int)(Math.random() * (producer.length))],
                country[(int)(Math.random() * (country.length))],
                pack[(int)(Math.random() * (pack.length))]);

        return this;
    }

    @Override
    public void read() {
        System.out.println(this);
    }

    @Override
    public void delete() {
        super.delete();
        setPack(null);
    }
}
