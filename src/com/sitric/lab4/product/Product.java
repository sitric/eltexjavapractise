package com.sitric.lab4.product;

/*
Product является родительским классом для класов Tea и Coffee
*/

import java.util.UUID;

public abstract class Product implements ICrudAction {
    private static int counter = 0;
    private UUID id;

    private String title;
    private double price;

    private String producer;
    private String country;

    public Product() {
    }

    public Product(String title, double price, String producer, String country) {
        this.title = title;
        this.price = price;
        this.producer = producer;
        this.country = country;

        counter++;
        id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return  "id=" + id +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", producer='" + producer + '\'' +
                ", country='" + country + ", ";
    }

    @Override
    public abstract void read();

    @Override
    public void update(String title, double price, String producer, String country) {
        setTitle(title);
        setPrice(price);
        setProducer(producer);
        setCountry(country);
    }

    @Override
    public abstract Product create();

    @Override
    public void delete() {
        setId(null);
        setTitle(null);
        setCounter(getCounter()-1);
        setCountry(null);
        setPrice(0.0);
        setProducer(null);
    }
}
