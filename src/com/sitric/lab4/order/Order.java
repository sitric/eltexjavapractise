package com.sitric.lab4.order;

/*
    класс "Заказ"
*/

import com.sitric.lab4.Credentials;
import com.sitric.lab4.ShoppingCart;

import java.util.Calendar;
import java.util.UUID;

public class Order {
    private String orderStatus; //в ожидании, обработан
    private long creationTime;
    private long waitingTime; // время, через которое заказ должен исчезнуть
    private UUID id;

    ShoppingCart cart;
    Credentials credentials;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Order(ShoppingCart cart, Credentials credentials) {
        this.orderStatus = "waiting";
        this.creationTime = Calendar.getInstance().getTimeInMillis();
        this.waitingTime = (long)(Math.random() * 1000);

        this.cart = cart;
        this.credentials = credentials;
        this.id = UUID.randomUUID();

    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public long getWaitingTime() {
        return waitingTime;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderStatus='" + orderStatus + '\'' +
                ", waiting time='" + waitingTime + '\'' +
                ", id=" + id +
                ", cart=" + cart +
                ", credentials=" + credentials +
                '}';
    }
}
