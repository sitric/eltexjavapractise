package com.sitric.lab4.order;

/*
    Класс автоматической генерации заказа
*/

import com.sitric.lab4.Credentials;
import com.sitric.lab4.ShoppingCart;
import com.sitric.lab4.product.Coffee;
import com.sitric.lab4.product.Product;
import com.sitric.lab4.product.Tea;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class OrderAutoGeneration implements Runnable{
    private Orders orders;
    private final Object LOCKER;

    public OrderAutoGeneration(Orders orders, Object LOCKER) {
        this.orders = orders;
        this.LOCKER = LOCKER;
    }

    private static Credentials generateCredentials() {
         List<Credentials> credentials = new ArrayList<>();
         credentials.add(new Credentials("Michael", "Caldwell", "C.", "masl@mail.ru"));
         credentials.add(new Credentials("David", "People", "S.", "kar@mail.ru"));
         credentials.add(new Credentials("Stephen", "Hines", "E.", "voyei@mail.ru"));

        return credentials.get((int)(Math.random()*credentials.size()));
    }

    private  Product generateProduct(){
        return Math.random() > 0.5 ? new Tea().create(): new Coffee().create();
    }

    private ShoppingCart generateCart(){
        ShoppingCart cart = new ShoppingCart();
        int productCount = (int)(Math.random() * 7 + 1);
        for (int i = 0; i < productCount; i++) {
            cart.add(generateProduct());
        }
        return cart;
    }

    private Order generate(){
        return new Order(generateCart(), generateCredentials());
    }

    private void doAutoOrder(){
        synchronized (LOCKER){
            Order order = generate();
            orders.makeOrder(order);
            System.out.println("--CREATE " + order);
        }
    }

    @Override
    public void run() {
        boolean goAhead = true;
        while (goAhead) {
            try {
                doAutoOrder();
                sleep(300);
            } catch (Exception e) {
                goAhead = false;
            }
        }
    }
}
