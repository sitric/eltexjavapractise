package com.sitric.lab4.order;

/*
    Класс Orders предоставляет функционал для работы с Order
*/

import java.util.LinkedList;
import java.util.List;

public class Orders{
    private List<Order> orders = new LinkedList<>();

    public List<Order> getOrders() {
        return orders;
    }

    public void makeOrder(Order order){
        orders.add(order);
    }

    public void showOrders() {
        for(Order order: orders){
            System.out.println(order);
            order.cart.showAll();
        }
    }

}
