package com.sitric.lab1;

/* Согласно условиям лабораторной работы #1 разработано приложение:
* 1. В качестве аргументов командной строки принимает: количество товара, тип товара (прим.: 3 tea).
* 2. Абстрактный класс Product с потомками: Tea и Coffee (список полей потомков согласно ТЗ)
* 3. В интерфейсе ICrudAction объявлены базовые методы для работы с потомками объектов Product'a
* 4. Для удобства создания объектов потомков разработан класс ProductsFactory
* 5. Идентификация сущностей в приложении производится посредством UUID.randomUUID()
* */

import com.sitric.lab1.product.Product;
import com.sitric.lab1.product.ProductsFactory;

public class Solution{

    public static void main(String[] args){
        Product[] products = new Product[Integer.parseInt(args[0])];

        for (int i = 0; i < Integer.parseInt(args[0]); i++) {
            products[i] = ProductsFactory.generate(args[1]);
        }

        for (Product product : products) {
            product.read();
        }
    }
}
