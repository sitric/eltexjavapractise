package com.sitric.lab1.product;

public class Coffee extends Product {
    private String bean_type;

    public Coffee(){}

    public Coffee(String title, double price, String producer, String country, String bean_type) {
        super(title, price, producer, country);
        this.bean_type = bean_type;
    }

    public String getBean_type() {
        return bean_type;
    }

    public void setBean_type(String bean_type) {
        this.bean_type = bean_type;
    }

    @Override
    public void read() {
        System.out.println(this);
    }

    @Override
    public void delete() {
        super.delete();
        setBean_type(null);
    }

    @Override
    public Coffee create() {
        String[] titles = {"Nescafe", "McCoffee", "Pele", "Cart Noire"};
        String[] producer = {"Lipton ltd", "Famous Tea Company", "Multon", "Nescafe Corp"};
        double[] price = {100.00, 150.00, 200.00, 250.00, 300.00};
        String[] country = {"Russia", "India", "China"};
        String[] bean_type = {"Arabica", "Robusta"};

        return new Coffee(titles[(int)(Math.random() * (titles.length))],
                price[(int)(Math.random() * (price.length))],
                producer[(int)(Math.random() * (producer.length))],
                country[(int)(Math.random() * (country.length))],
                bean_type[(int)(Math.random() * (bean_type.length))]);
    }

    public void update(String title, double price, String producer, String country, String bean_type) {
        super.update(title, price, producer, country);
        setBean_type(bean_type);
    }

    @Override
    public String toString() {
        return "Coffee{" + super.toString() +
                "bean_type='" + bean_type + '\'' +
                '}';
    }
}
