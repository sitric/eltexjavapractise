package com.sitric.lab6.server;

import com.sitric.lab6.Connection;
import com.sitric.lab6.order.Order;
import com.sitric.lab6.order.Orders;

import java.io.IOException;
import java.net.Socket;
/*
    Данный класс устанавливает соединение с клиентом посредством создания экземпляра
    класса Connection и принимает от него заказ
 */
public class IncomingOrdersHandler implements Runnable {
    private Socket incoming;
    private Orders orders;

    IncomingOrdersHandler(Socket incoming, Orders orders) {
        this.incoming = incoming;
        this.orders = orders;
    }

    @Override
    public void run(){
        try{

            try (Connection connection = new Connection(incoming)){
                Order order = connection.receive();
                int portToSendOrderBack = Integer.parseInt(connection.receiveMessage());

                orders.makeBindedOrder(order, portToSendOrderBack);

                System.out.println();
                System.out.println("--ORDER WITH ID: " + order.getId() + " ADDED TO PROCESSING");

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
