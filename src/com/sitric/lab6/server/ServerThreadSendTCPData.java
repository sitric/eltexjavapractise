package com.sitric.lab6.server;

import com.sitric.AppConst;

import java.io.IOException;
import java.net.*;

import static java.lang.Thread.sleep;

/*
    Класс - поток сервера, который периодически рассылает данные для связи по TCP используя протокол UDP
    A server thread that continuously sends data for connecting a TCP using UDP socket connection
*/
public class ServerThreadSendTCPData implements Runnable {
    private InetAddress IPAddress;

    ServerThreadSendTCPData(InetAddress IPAddress) {
        this.IPAddress = IPAddress;
    }

    @Override
    public void run() {
        boolean goAhead = true;
        System.out.println("--SENDING DATA FOR CONNECT BY TCP USING UDP");

        while (goAhead) {
            try {
                DatagramSocket serverSocket = new DatagramSocket();
                byte[] sendData = (IPAddress + " " + AppConst.TCP_PORT).getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, AppConst.UDP_PORT);
                serverSocket.send(sendPacket);
                sleep(500);

            } catch (UnknownHostException e) {
                // неверный адрес получателя
                System.out.println("--error! Unavailable host");
                e.printStackTrace();
            } catch (SocketException e) {
                // возникли ошибки при передаче данных
                System.out.println("--error during data transfer");
                e.printStackTrace();
            } catch (InterruptedException ie) {
                System.out.println("-- interrupted exception");
            } catch (IOException e) {
                System.out.println("--other server error");
                e.printStackTrace();
            }
        }
    }
}
