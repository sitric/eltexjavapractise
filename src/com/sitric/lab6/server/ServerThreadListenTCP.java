package com.sitric.lab6.server;

/*
    Класс принимающий заказы от клиентов по TCP
 */
import com.sitric.AppConst;
import com.sitric.lab6.order.CheckPending;
import com.sitric.lab6.order.Order;
import com.sitric.lab6.order.Orders;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


public class ServerThreadListenTCP implements Runnable{
    private InetAddress IPAddress;

    private Orders orders = new Orders();
    private List<Order> ordersInList = new ArrayList<>();


    public InetAddress getIPAddress() {
        return IPAddress;
    }
    public List<Order> getOrders() {
        return ordersInList;
    }


    ServerThreadListenTCP(InetAddress IPAddress) {
        this.IPAddress = IPAddress;
        final Object LOCKER = new Object();

        Thread pendingThread = new Thread(new CheckPending(orders, LOCKER, IPAddress));
        pendingThread.start();

    }

    void listenTCP() {
        try (ServerSocket ss = new ServerSocket(AppConst.TCP_PORT)){

            //В бесконечном цикле слушает и принимает входящие сокетные соединения
            Socket socket = ss.accept();

            // на каждого клиента создаем поток-обработчик
            Thread clientsHandler = new Thread(new IncomingOrdersHandler(socket, orders));
            clientsHandler.start();

        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean goAhead = true;
        System.out.println("--LISTENING TCP");
        while (goAhead) {
            listenTCP();
        }
    }
}
