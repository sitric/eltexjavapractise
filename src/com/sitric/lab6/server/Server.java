package com.sitric.lab6.server;
/*
    Основной класс сервера
*/
import java.io.*;
import java.net.*;

public class Server {
    public static void main(String[] args) throws IOException {
        InetAddress IPAddress = InetAddress.getByName("127.0.0.1");

        //Поток, рассылающий по UDP данные для коннекта по TCP
        Thread sendTCPData = new Thread(new ServerThreadSendTCPData(IPAddress));
        //Поток, принимающий заказы от клиентов по TCP
        Thread listenTCP = new Thread(new ServerThreadListenTCP(IPAddress));


        sendTCPData.start();
        listenTCP.start();

    }
}
