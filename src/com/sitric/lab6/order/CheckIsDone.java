package com.sitric.lab6.order;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

public class CheckIsDone extends ACheck implements Runnable {
    private Orders orders;
    private final Object LOCKER;

    public CheckIsDone(Orders orders, Object LOCKER) {
        this.orders = orders;
        this.LOCKER = LOCKER;
    }

    @Override
    void checkStatus() {
        synchronized (LOCKER) {
            List<Order> ordersToDelete = new ArrayList<>();
            for (Order order : orders.getOrders()) {
                if (order.getOrderStatus().equals("done")) {
                    ordersToDelete.add(order);
                    System.out.println("--DELETE ORDER ID: " + order.getId());

                }
            }
            if (ordersToDelete.size() > 0) {
                orders.getOrders().removeAll(ordersToDelete);
            }
        }
    }

    @Override
    public void run() {
        boolean goAhead = true;
        while (goAhead) {
            try {
                if (orders.getOrders().size() > 0) {
                    checkStatus();
                }
                sleep(200);
            } catch (Exception e) {
                goAhead = false;
            }
        }
    }
}
