package com.sitric.lab6.order;
/*
  Данный класс занимается обходом LinkedHashMap<Order, portNumber> и проверяет можно ли пометить заказ как "исполнен"
*/

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.Thread.sleep;

public class CheckPending extends ACheck implements Runnable {
    private Orders orders;
    private InetAddress IPAddress;
    private final Object LOCKER;



    public CheckPending(Orders orders, Object LOCKER, InetAddress IPAddress) {
        this.orders = orders;
        this.LOCKER = LOCKER;
        this.IPAddress = IPAddress;

    }


    @Override
    public void checkStatus() {
        synchronized (LOCKER) {
            ConcurrentHashMap<Order, Integer> ordersCopyMap = orders.getBindedOrders();
            for(Map.Entry<Order, Integer> pair: orders.getBindedOrders().entrySet()) {
                if (pair.getKey().getOrderStatus().equals("waiting") &&
                        (pair.getKey().getCreationTime() + pair.getKey().getWaitingTime() <= Calendar.getInstance().getTimeInMillis())
                        ) {
                    System.out.println("--UPDATE STATUS ORDER ID: " + pair.getKey().getId());
                    pair.getKey().setOrderStatus("done");
                    ordersCopyMap.remove(pair.getKey());
                    sendCheckedOrderToClient(pair.getValue(), pair.getKey());

                }
            }
            // удаляем обработанные заказы
            orders.setBindedOrders(ordersCopyMap);

        }
    }

    // метод отправляет исполненные заказы клиенту

    void sendCheckedOrderToClient(Integer port, Order order){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(order);
            byte[] buff = baos.toByteArray();
            DatagramSocket serverSocket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(buff, buff.length, IPAddress, port);

            serverSocket.send(packet);
            System.out.println("--SENDING ORDER BACK TO CLIENT");
            System.out.println();
        } catch (IOException e) {
            System.out.println("--SERVER ERROR WHILE TRYING SERIALIZE ORDER OBJECT");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean goAhead = true;
        while (goAhead) {
            try {
                if (orders.getBindedOrders().size() > 0) {
                    checkStatus();
                }
                sleep(2000);
            } catch (Exception e) {
                goAhead = false;
            }
        }
    }
}
