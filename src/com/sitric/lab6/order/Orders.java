package com.sitric.lab6.order;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class Orders{

    private ConcurrentHashMap<Order, Integer> bindedOrders = new ConcurrentHashMap<>();

    public ConcurrentHashMap<Order, Integer> getBindedOrders() {
        return bindedOrders;
    }

    public void setBindedOrders(ConcurrentHashMap<Order, Integer> bindedOrders) {
        this.bindedOrders = bindedOrders;
    }

    private List<Order> orders = new LinkedList<>();

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void makeOrder(Order order){
        orders.add(order);
    }

    public void makeBindedOrder(Order order, Integer port){
        bindedOrders.put(order, port);
    }

    public void showOrders() {
        for(Order order: orders){
            System.out.println(order);
            order.getShoppingCart().showAll();
        }
    }

    public Order findOrderById(UUID id) {
        Order order = null;

        for(Order searchObj : orders) {
            if (searchObj.getId().equals(id)){
                order = searchObj;
                break;
            }
        }
        return order;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orders=" + orders +
                '}';
    }
}
