package com.sitric.lab6.product;

public interface ICrudAction {
    Product create();
    void read();
    void update(String title, double price, String producer, String country);
    void delete();
}
