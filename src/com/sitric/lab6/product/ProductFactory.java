package com.sitric.lab6.product;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProductFactory {
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public static Product build(String goodsType){
        Product product = null;
        if (goodsType.equalsIgnoreCase("tea")){
            Tea tea = new Tea(null, 0.0, null, null, null);
            product = tea.create();
        }
        else if (goodsType.equalsIgnoreCase("coffee")){
            Coffee coffee = new Coffee(null, 0.0, null, null, null);
            product = coffee.create();
        }
        return product;
    }

    Tea createTea(String title, double price, String producer, String country) throws IOException {
        System.out.println("Введите тип упаковки:");
        String pack = br.readLine();
        Tea tea = new Tea(null, 0.0, null, null, null);
        tea.update(title, price, producer, country, pack);

        return tea;
    }

    Coffee createCoffee(String title, double price, String producer, String country) throws IOException {
        System.out.println("Введите тип зёрен:");
        String bean_type = br.readLine();
        Coffee coffee = new Coffee(null, 0.0, null, null, null);
        coffee.update(title, price, producer, country, bean_type);

        return coffee;
    }
}
