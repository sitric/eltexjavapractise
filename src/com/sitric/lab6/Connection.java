package com.sitric.lab6;

import com.sitric.lab6.order.Order;

import java.io.*;
import java.net.Socket;
import java.net.SocketAddress;

/*
    Класс соединения между клиентом и сервером

    Создать объект класса ObjectOutputStream нужно до того, как будет создаваться объект
    класса ObjectInputStream, иначе может возникнуть взаимная блокировка потоков,
    которые хотят установить соединение через класс Connection
*/

public class Connection implements Closeable {
    private final Socket socket;
    private final ObjectOutputStream out;
    private final ObjectInputStream in;

    public Connection(Socket socket) throws IOException{
        this.socket = socket;
        this.out = new ObjectOutputStream(socket.getOutputStream());
        this.in = new ObjectInputStream(socket.getInputStream());
    }

    void sendOrder(Order order) throws IOException {
        synchronized (out) {
            out.writeObject(order);
        }
    }

    void sendMessage (String message) throws IOException {
        synchronized (out) {
            socket.getOutputStream().write(message.getBytes());
        }
    }

    /* читает (десериализовывает) данные из ObjectInputStream */
    public Order receive () throws IOException, ClassNotFoundException {
        synchronized (in) {
            return (Order) in.readObject();
        }
    }

    public String receiveMessage () throws IOException{
        synchronized (in) {
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            return br.readLine();
        }
    }

    /* закрывает все ресурсы класса */
    public void close() throws IOException {
        socket.close();
        out.close();
        in.close();
    }
}

