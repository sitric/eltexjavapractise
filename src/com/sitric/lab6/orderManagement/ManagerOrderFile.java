package com.sitric.lab6.orderManagement;

/*
*
* Класс, предоставляющий методы для записи в файл и извлечения из файла заказов в бинарном виде
*
*/

import com.sitric.AppConst;
import com.sitric.lab6.order.Order;
import com.sitric.lab6.order.Orders;

import java.io.*;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import static java.io.File.separator;

public class ManagerOrderFile implements IOrder {

    static String filepath = Paths.get("").toAbsolutePath().toString() + separator + "log";

    final static File prepareLogFile() throws IOException{
        File logDir = new File(filepath);
        File logFile = new File(filepath + separator + AppConst.BINARY_FILENAME);

        if (!logDir.exists()){
            logDir.mkdirs();
        }

        if (!logFile.exists()){
            logFile.createNewFile();
        }
        return logFile;
    }

    @Override
    public Order readById(UUID id) {
        Order order = null;
        List<Order> ordersFromFile = readAll();
        for (Order searchOrder : ordersFromFile) {
            if (searchOrder.getId().equals(id)) {
                order = searchOrder;
                System.out.println("--order with ID: " + id + " successfully loaded");
                break;
            }
        }
        if (order == null) {
            System.out.println("--order with ID: " + id + " not found");
        }
        return order;
    }

    @Override
    public void saveById(Orders orders, UUID id) {
        Order order = orders.findOrderById(id);
        if (order != null) {
            try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(prepareLogFile(), true)))
            {
                oos.writeObject(order);
                System.out.println("--order with ID: " + id + " successfully saved");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("--order with ID: " + id + " not found");
        }
    }
    @SuppressWarnings("unchecked")
    public List<Order> readAll(){
        List<Order> ordersList = null;
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(prepareLogFile()))){
            ordersList = (List<Order>)ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("-- error during read data from binary file");
            e.printStackTrace();
        }
        return ordersList;
    }

    @Override
    public void saveAll(List<Order> orders){
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(prepareLogFile(), false)))
        {
            oos.writeObject(orders);
            System.out.println("--all objects successfully saved");
        } catch (IOException e) {
            System.out.println("--error during saving objects");
            e.printStackTrace();
        }
    }
}
