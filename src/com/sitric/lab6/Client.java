package com.sitric.lab6;
/*
  Класс клиента.
  Получает данные по UDP о порте и адресе для TCP соединения.
  Генерирует заказ.
  Связывается по TCP с сервером и направляет заказ на обработку.
  Закрывает TCP соединение.
  Принимает обработанный заказ по UDP.
  Формирует заказ повторно
 */
import com.sitric.AppConst;
import com.sitric.lab6.order.Order;
import com.sitric.lab6.order.OrderAutoGeneration;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;
import java.util.Date;

public class Client {
    private byte[] receiveData = new byte[AppConst.BUFFER_SIZE];

    public static void main(String[] args) {
        Client client = new Client();
        client.receiveData();
    }

    public void receiveData() {
        try {
            DatagramSocket clientSocket = new DatagramSocket(AppConst.UDP_PORT);
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

            clientSocket.setSoTimeout(7000);
            // Ожидаем получение пакета с данными для TCP соединения и закрываем UDP
            clientSocket.receive(receivePacket);
            clientSocket.close();

            System.out.println("--TCP DATA RECEIVED");
            System.out.println();
            String dataToTCP = new String(receivePacket.getData()).trim();
            String[] connectionData = dataToTCP.split(" ");

            InetAddress TCPServerAddr = InetAddress.getByName(connectionData[0].substring(1, connectionData[0].length()));
            int TCPPort = Integer.parseInt(connectionData[1]);

            // устанавливаем соединение по TCP и начинаем генерировать заказы
            while(true) {
                sendOrderToServer(TCPServerAddr, TCPPort);
            }

        } catch (SocketTimeoutException ste) {
            System.out.println("SocketTimeoutException");
        }
        catch (IOException e) {
            System.out.println("--ERROR WHILE RECEIVING TCP CONNECTION DATA");
            e.printStackTrace();
        }
    }

    public void sendOrderToServer(InetAddress serverAddress, int port){
        try {
            //Создаем TCP соединение
            Socket socket = new Socket(serverAddress, port);
            DatagramSocket datagramSocket = new DatagramSocket();
            int portToGetOrdersBack = datagramSocket.getLocalPort();
            Connection connection = new Connection(socket);
            System.out.println("--GENERATE NEW ORDER");

            Order orderForServer = new OrderAutoGeneration().generate();

            //отправляем заказ в обработку
            connection.sendOrder(orderForServer);
            connection.sendMessage("" + portToGetOrdersBack);
            System.out.println("--ORDER WITH ID: " + orderForServer.getId() + " SENT TO SERVER");

            //закрываем TCP соединение
            connection.close();
            DatagramPacket datagramPacket = new DatagramPacket(receiveData, receiveData.length);

            //выставляем таймаут для получения обработанного заказа
            datagramSocket.setSoTimeout(7000);

            //ожидаем получение поката с данными для TCP соединения
            datagramSocket.receive(datagramPacket);

            byte[] data = datagramPacket.getData();
            datagramSocket.close();

            ByteArrayInputStream bais = new ByteArrayInputStream(data);
            ObjectInputStream ois = new ObjectInputStream(bais);

            try {
                Order receivedOrder = (Order) ois.readObject();
                System.out.println("--ORDER RECEIVED " + receivedOrder.getId() + " " + new Date());
                System.out.println();
                //закрываем все потоки

                bais.close();
                ois.close();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        } catch (SocketTimeoutException ste) {
            System.out.println("--CLIENT: TIMEOUT WHILE WAITING ORDER BACK");
            System.exit(0);
        } catch (IOException e) {
            /*e.printStackTrace();*/
        }
    }
}
