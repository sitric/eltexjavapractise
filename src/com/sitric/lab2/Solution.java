package com.sitric.lab2;

/* Согласно условиям лабораторной работы #2 (является продолжением ЛР#1) реализовано:
 * 1. Класс Credentials - персональные данные покупателя
 * 2. Класс ShoppingCart - корзина
 * 3. Класс Order - заказ, связанный с классами Credentials и ShoppingCart посредством агрегации
 * */

import com.sitric.lab1.product.Product;
import com.sitric.lab1.product.ProductsFactory;

public class Solution{

    public static void main(String[] args){
        Product[] products = new Product[Integer.parseInt(args[0])];

        for (int i = 0; i < Integer.parseInt(args[0]); i++) {
            products[i] = ProductsFactory.generate(args[1]);
        }

        for (Product product : products) {
            product.read();
        }
    }
}
