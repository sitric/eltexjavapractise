package com.sitric.lab2.order;

/*
    Класс Orders предоставляет функционал для работы с Order
 */
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class Orders {
    private List<Order> orders = new LinkedList<>();

    public void makeOrder(Order order){
        orders.add(order);
    }

    public void checkIfOrdersDone(){
        List<Order> ordersCopy = orders;
        for (Order order : orders){

            if (Calendar.getInstance().getTimeInMillis() < (order.getCreationTime() + order.getWaitingTime())){

                ordersCopy.remove(order);
            }
        }
        orders = ordersCopy;
    }

    public void showOrders() {
        for(Order order: orders){
            System.out.println("Order ID:" + order.getId());
            System.out.println("Customer details: ");
            System.out.println(order.credentials);
            System.out.println("Cart details:");
            order.cart.showAll();
        }
    }

}
