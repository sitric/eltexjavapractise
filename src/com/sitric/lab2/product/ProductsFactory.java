package com.sitric.lab2.product;

public class ProductsFactory {
    public static Product generate(String goodsType) {
        Product product = null;
        if (goodsType.equalsIgnoreCase("tea")){
            product = new Tea().create();
        }
        else if (goodsType.equalsIgnoreCase("coffee")){
            product = new Coffee().create();
        }
        return product;
    }
}
