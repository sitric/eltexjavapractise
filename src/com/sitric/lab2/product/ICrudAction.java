package com.sitric.lab2.product;

public interface ICrudAction {
    // согласно ТЗ метод create заполняет поля товара случайными значениями
    Product create();
    void read();
    void update(String title, double price, String producer, String country);

    // согласно ТЗ метод delete обнуляет все поля объекта и декрементирует счетчик - количество товара
    void delete();
}
