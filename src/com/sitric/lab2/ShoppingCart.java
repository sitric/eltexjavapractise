package com.sitric.lab2;

import com.sitric.lab2.product.Product;

import java.util.*;

/*
    Класс-коллекция "корзина"
    считается, что корзина может сущестовать без связи с заказом
*/

public class ShoppingCart {

    private List<Product> cartItems = new ArrayList<>();

    public ShoppingCart() {
    }

    public void add(Product product){
        cartItems.add(product);
    }

    public void delete(Product product){
        cartItems.remove(product);
    }

    public void showAll(){
        for(Product product : cartItems) {
            product.read();
        }
    }
}
