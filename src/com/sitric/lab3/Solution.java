package com.sitric.lab3;

/*
 * Согласно условиям лабораторной работы #3 (является продолжением ЛР#2) реализовано:
 * 1. Класс ShoppingCart - реализован в виде Generic-класса
 * 2. Создан тестовый заказ
 * */

import com.sitric.lab3.product.ProductsFactory;
import com.sitric.lab3.order.Order;
import com.sitric.lab3.order.Orders;
import com.sitric.lab3.product.Product;

import java.util.ArrayList;

public class Solution{

    public static void main(String[] args){
        Product[] products = new Product[Integer.parseInt(args[0])];

        for (int i = 0; i < Integer.parseInt(args[0]); i++) {
            products[i] = ProductsFactory.generate(args[1]);
        }

        Credentials credentials = new Credentials("Ivan", "Abramov", "Nikolayevich", "ivan@mail.ru");
        ShoppingCart cart = new ShoppingCart();
        cart.add(products[0]);
        cart.add(products[1]);

        Order order = new Order(cart, credentials);
        Orders orders = new Orders();
        orders.makeOrder(order);

        orders.showOrders();

    }

}
