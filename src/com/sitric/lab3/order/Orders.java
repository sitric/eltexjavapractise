package com.sitric.lab3.order;

/*
    Класс Orders предоставляет функционал для работы с Order
 */

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class Orders <T extends Order>{
    private List<T> orders = new LinkedList<>();

    public void makeOrder(T order){
        orders.add(order);
    }

    public void checkIfOrdersDone(){ // если время ожидания истекло
        List<T> ordersCopy = orders;
        for (Order order : orders){
            if (Calendar.getInstance().getTimeInMillis() < (order.getCreationTime() + order.getWaitingTime())){
                ordersCopy.remove(order);
            }
        }
        orders = ordersCopy;
    }

    public void showOrders() {
        for(Order order: orders){
            System.out.println("Order ID: " + order.getId());
            System.out.println("Order status: " + order.getOrderStatus());
            System.out.println("Customer details: ");
            System.out.println(order.credentials);
            System.out.println("Cart details:");
            order.cart.showAll();
        }
    }

}
