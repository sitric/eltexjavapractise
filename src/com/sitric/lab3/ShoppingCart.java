package com.sitric.lab3;

import com.sitric.lab3.product.Product;

import java.util.*;

/*
    класс-коллекция "корзина"
    считается, что корзина может сущестовать без связи с заказом
*/

public class ShoppingCart<T extends Product> {

    private List<T> cartItems = new ArrayList<>();
    private Set<UUID> ids = new HashSet<>();

    public ShoppingCart() {
    }

    public List<T> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<T> cartItems) {
        this.cartItems = cartItems;
    }

    public Set<UUID> getIds() {
        return ids;
    }

    public void setIds(Set<UUID> ids) {
        this.ids = ids;
    }

    public void add(T product){
        cartItems.add(product);
    }

    public void delete(T product){
        cartItems.remove(product);
    }

    public boolean findProductById(UUID id){
        return ids.contains(id);
    }

    public void showAll(){
        for(Product product : cartItems) {
            product.read();
        }
    }
}
