package com.sitric;

public final class AppConst {
    public static final String BINARY_FILENAME = "orders_log.bin";
    public static final String JSON_FILENAME = "orders_log.json";
    public static final int UDP_PORT = 8033;
    public static final int UDP_PORT_ORDER = 11111;
    public static final int TCP_PORT = 5100;
    public static final int BUFFER_SIZE = 4096;
}
