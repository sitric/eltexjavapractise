## Summary

 This repository is the result of performed laboratory works of the summer programming school Eltex.
 In the **com.sitric** directory there are folders **lab1-lab6** in each of which the job is performed according to the technical task [link](http://www.c-java.ru/wp-content/uploads/2017/09/Java_JavaEE.txt) (variant 1).
 Objectives of laboratory work:
 1-2. Introduction to the principles of object-oriented programming in Java: a description of the subject area in the form of an owl
 3. Generics
 4. Running the program in multithreaded mode
 5. Working with files. Presentation of data in binary form and format JSON
 6. Work with sockets. Create a client-server application with the ability to connect multiple clients.
 
 More detailed information is contained in the class **Solution** of each of the **lab1-lab6** folders.
 
## О репозитории

Данный репозиторий это результат выполненных лабораторных работ летней школы программирования "Элтекс". 
В каталоге **com.sitric** находятся папки **lab1-lab6** в каждой из которых выполненное задание согласно технического задания [ссылка](http://www.c-java.ru/wp-content/uploads/2017/09/Java_JavaEE.txt) (вариант 1).
Цели лабораторных работ:
1-2. Знакомство с принципами объектно-ориентированного программирования в Java: описание предметной области в виде совокупности классов и интерфейсов. Настройка взаимодействия между ними. 
3. Дженерики
4. Выполнение программы в многопоточном режиме
5. Работа с файлами. Представление данных в бинарном виде и формате JSON
6. Работа с сокетами. Создание клиент-серверного приложения с возможностью подключения нескольких клиентов.

Более подробная информация содержится в классе **Solution** каждой из папок **lab1-lab6**
